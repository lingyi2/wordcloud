from turtle import *
speed(100)

penup()
goto(-300,270)
stopwords=["in","a","the","i"]

words_count = 0
size = 60
str = ""
dict = {}
with open("long.txt", "r") as f:
    for line in f:
        line = line.replace("\n", " ")
        line = line.replace(",", " ")
        line = line.replace(".", " ")
        line = line.replace("!", " ")
        line = line.lower()
        words_list = line.split()
        number_of_words_in_line = len(words_list)
        words_count = words_count + number_of_words_in_line
        str = str + line
        for word in words_list:
            dict.setdefault(word, 0)
            dict[word] = dict[word] + 1


def count_words(str):
    words = {}
    str_words = str.split()
    for word in str_words:
        words.setdefault(word, 0)
        words[word] = words[word] + 1
    return words

commoncount = 0


for key in sorted(dict, key=dict.get, reverse=True):
    commoncount += 1
    size -= 2
    if key in stopwords:
        pass
    elif (commoncount <= 20):
        write(key, dict[key],font=("Time New Roman", size, "normal"))
        penup()
        goto(-300,270-(39*commoncount))
    else:
        break



#for i in range(words_count):
      #write(count_words(str))
done()
